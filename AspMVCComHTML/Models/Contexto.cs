﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AspMVCComHTML.Models
{
    public class Contexto : DbContext
    {
        public DbSet<Aluno> Alunos { get; set; }
    }
}