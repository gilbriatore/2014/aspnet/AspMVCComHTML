﻿using AspMVCComHTML.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspMVCComHTML.Controllers
{
    public class AlunoController : Controller
    {
        Contexto ctx = new Contexto();

        public ActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Incluir(Aluno aluno)
        {
            ctx.Alunos.Add(aluno);
            ctx.SaveChanges();
            return Redirect("/Inicio/Inicial");
        }

        public ActionResult Listar()
        {
            List<Aluno> alunos = ctx.Alunos.ToList();
            return View(alunos);
        }

        public ActionResult Editar(int id)
        {
            Aluno aluno = ctx.Alunos.Find(id);
            return View(aluno);
        }

        [HttpPost]
        public ActionResult Editar(Aluno aluno)
        {
            ctx.Entry(aluno).State = System.Data.Entity.EntityState.Modified;
            ctx.SaveChanges();
            return Redirect("/Inicio/Inicial");
        }

        [HttpGet]
        public ActionResult Excluir(int id)
        {
            Aluno aluno = ctx.Alunos.Find(id);
            return View(aluno);
        }

        [HttpPost]
        public ActionResult Excluir(Aluno aluno)
        {
            ctx.Entry(aluno).State = System.Data.Entity.EntityState.Deleted;
            ctx.SaveChanges();
            return Redirect("/Inicio/Inicial");
        }

    }
}
