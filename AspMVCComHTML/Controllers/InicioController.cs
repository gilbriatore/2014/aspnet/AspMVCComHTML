﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspMVCComHTML.Controllers
{
    public class InicioController : Controller
    {
        public ActionResult Inicial()
        {
            return View();
        }
    }
}
